import json

import pytest

from src.api.users.models import User


def test_add_user(test_app, test_database):
    # given
    client = test_app.test_client()
    new_user_email = "david@autolab.com"

    # when
    res = client.post(
        "/users",
        data=json.dumps({"username": "david", "email": new_user_email}),
        content_type="application/json",
    )
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 201
    assert f"{new_user_email} was added!" in data["message"]


def test_add_user_invalid_json(test_app, test_database):
    # given
    client = test_app.test_client()

    # when
    res = client.post(
        "/users",
        data=json.dumps({}),
        content_type="application/json",
    )
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_invalid_json_keys(test_app, test_database):
    # given
    client = test_app.test_client()

    # when
    res = client.post(
        "/users",
        data=json.dumps({"email": "david@autolab.com"}),
        content_type="application/json",
    )
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 400
    assert "Input payload validation failed" in data["message"]


def test_add_user_duplicate_email(test_app, test_database):
    # given
    client = test_app.test_client()
    new_user_username = "david"
    new_user_email = "david@autolab.com"

    # when
    client.post(
        "/users",
        data=json.dumps({"username": new_user_username, "email": new_user_email}),
        content_type="application/json",
    )
    res = client.post(
        "/users",
        data=json.dumps({"username": new_user_username, "email": new_user_email}),
        content_type="application/json",
    )
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 400
    assert "Sorry. That email already exists." in data["message"]


def test_single_user(test_app, test_database, add_user):
    # given
    new_user_username = "david"
    new_user_email = "david@autolab.com"
    user = add_user(username=new_user_username, email=new_user_email)

    # when
    client = test_app.test_client()
    res = client.get(f"/users/{user.id}")
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 200
    assert new_user_username in data["username"]
    assert new_user_email in data["email"]


def test_single_user_incorrect_id(test_app, test_database):
    # given
    client = test_app.test_client()

    # when
    res = client.get("/users/999")
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 404
    assert "User 999 does not exist" in data["message"]


def test_all_users(test_app, test_database, add_user):
    # given
    new_user_username_1 = "david"
    new_user_email_1 = "david@autolab.com"
    new_user_username_2 = "granados"
    new_user_email_2 = "granados@autolab.com"
    add_user(new_user_username_1, new_user_email_1)
    add_user(new_user_username_2, new_user_email_2)
    client = test_app.test_client()

    # when
    res = client.get("/users")
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 200
    assert len(data) == 2
    assert new_user_username_1 in data[0]["username"]
    assert new_user_email_1 in data[0]["email"]
    assert new_user_username_2 in data[1]["username"]
    assert new_user_email_2 in data[1]["email"]


def test_remove_user(test_app, test_database, add_user):
    # given
    user_to_be_delete_username = "david"
    user_to_be_delete_email = "david@autolab.com"
    test_database.session.query(User).delete()
    user = add_user(user_to_be_delete_username, user_to_be_delete_email)
    client = test_app.test_client()

    # when
    res_one = client.get("/users")
    data = json.loads(res_one.data.decode())

    # then
    assert res_one.status_code == 200
    assert len(data) == 1

    # when
    res_two = client.delete(f"/users/{user.id}")
    data = json.loads(res_two.data.decode())

    # then
    assert res_two.status_code == 200
    assert f"{user_to_be_delete_email} was removed!" in data["message"]

    # when
    res_three = client.get("/users")
    data = json.loads(res_three.data.decode())

    # then
    assert res_three.status_code == 200
    assert len(data) == 0


def test_remove_user_incorrect_id(test_app, test_database):
    # given
    client = test_app.test_client()

    # when
    res = client.delete("/users/999")
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 404
    assert "User 999 does not exist" in data["message"]


def test_update_user(test_app, test_database, add_user):
    # given
    user_to_be_updated_username = "david"
    user_to_be_updated_email = "david@autolab.com"
    username_for_update = "granados"
    email_for_update = "granados@autolab.com"
    user = add_user(user_to_be_updated_username, user_to_be_updated_email)
    client = test_app.test_client()

    # when
    res_one = client.put(
        f"/users/{user.id}",
        data=json.dumps({"username": username_for_update, "email": email_for_update}),
        content_type="application/json",
    )
    data = json.loads(res_one.data.decode())
    # then
    assert res_one.status_code == 200
    assert f"{user.id} was updated!" in data["message"]

    # when
    res_two = client.get(f"/users/{user.id}")
    data = json.loads(res_two.data.decode())
    # then
    assert res_two.status_code == 200
    assert username_for_update in data["username"]
    assert email_for_update in data["email"]


@pytest.mark.parametrize(
    "user_id, payload, status_code, message",
    [
        [1, {}, 400, "Input payload validation failed"],
        [1, {"email": "david@autolab.com"}, 400, "Input payload validation failed"],
        [
            999,
            {"username": "david", "email": "david@autolab.com"},
            404,
            "User 999 does not exist",
        ],
    ],
)
def test_update_user_invalid(
    test_app, test_database, user_id, payload, status_code, message
):
    # given
    client = test_app.test_client()

    # when
    res = client.put(
        f"/users/{user_id}",
        data=json.dumps(payload),
        content_type="application/json",
    )
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == status_code
    assert message in data["message"]


def test_update_user_duplicate_email(test_app, test_database, add_user):
    # given
    user_1_username = "david"
    user_1_email = "david@autolab.com"
    user_2_username = "granados"
    user_2_email = "granados@autolab.com"
    add_user(user_1_username, user_1_email)
    user_2 = add_user(user_2_username, user_2_email)

    client = test_app.test_client()

    # when
    res = client.put(
        f"/users/{user_2.id}",
        data=json.dumps({"username": user_2_username, "email": user_1_email}),
        content_type="application/json",
    )
    data = json.loads(res.data.decode())

    # then
    assert res.status_code == 400
    assert "Sorry. That email already exists." in data["message"]
