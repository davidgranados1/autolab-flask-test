
[![pipeline status](https://gitlab.com/davidgranados1/autolab-flask-test/badges/develop/pipeline.svg)](https://gitlab.com/davidgranados1/autolab-flask-test/commits/develop)

### Build app
```$ docker-compose build```
### Run app
```$ docker-compose up -d```
### Stop the containers
```$ docker-compose stop```
### Bring down the containers
```$ docker-compose down```
### Bring down the containers and volumes
```$ docker-compose down -v```
### Force a build
```$ docker-compose build --no-cache```
### Remove images
```$ docker rmi $(docker images -q)```
### Create db
```$ docker-compose exec api python manage.py recreate_db```
### Seed db
```$ docker-compose exec api python manage.py seed_db```
### Connect to db
```$ docker-compose exec api-db psql -U postgres```
### Sort Imports check
```$ docker-compose exec api isort src --check-only```
### Sort Imports diff
```$ docker-compose exec api isort src --diff```
### Sort Imports
```$ docker-compose exec api isort src```
### Format check
```$ docker-compose exec api black src --check```
### Format diff
```$ docker-compose exec api black src --diff```
### Format
```$ docker-compose exec api black src```
### Lint
```$ docker-compose exec api flake8 src```
### Run tests
```$ docker-compose exec api python -m pytest "src/tests"```
### Run tests
```$ docker-compose exec api python -m pytest "src/tests"```
### Run users unit tests in parallel
```$ docker-compose exec api python -m pytest "src/tests/unit/test_users_unit.py" -p no:warnings -k "unit" -n auto```
### Run tests with coverage and generate html report
```$ docker-compose exec api python -m pytest "src/tests" -p no:warnings --cov="src" --cov-report html```
### Tests commands

- normal run

`````$ docker-compose exec api python -m pytest "src/tests"`````

- disable warnings

```$ docker-compose exec api python -m pytest "src/tests" -p no:warnings```

- run only the last failed tests

```$ docker-compose exec api python -m pytest "src/tests" --lf```

- run only the tests with names that match the string expression

```$ docker-compose exec api python -m pytest "src/tests" -k "config and not test_development_config"```

- stop the test session after the first failure

```$ docker-compose exec api python -m pytest "src/tests" -x```

- enter PDB after first failure then end the test session

```$ docker-compose exec api python -m pytest "src/tests" -x --pdb```

- stop the test run after two failures

```$ docker-compose exec api python -m pytest "src/tests" --maxfail=2```

- show local variables in tracebacks

```$ docker-compose exec api python -m pytest "src/tests" -l```

- list the 2 slowest tests

```$ docker-compose exec api python -m pytest "src/tests" --durations=2```

